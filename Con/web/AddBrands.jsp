<%-- 
    Document   : AddBrands
    Created on : Jul 29, 2014, 6:45:34 PM
    Author     : rohitsingh
--%>

<%@page import="Model.Category"%>
<%@page import="Model.BRAND"%>
<%@page import="java.sql.ResultSet"%>
<%@page import="java.sql.PreparedStatement"%>
<%@page import="java.sql.DriverManager"%>
<%@page import="java.sql.Connection"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
       <link rel="stylesheet" type="text/css" href="style.css" />


 
</head>
<body>
<%@ include file="AdminHeader.jsp" %>

    <div class="center_content">  
        <form action="ADDBRANDS" method="post" enctype="multipart/form-data">
<table border="0"  cellpadding="5" cellspacing="0" width="600">
    <CAPTION><EM>ADD BRANDS</EM></CAPTION>
    <tr>
<td><b>Brand ID*:</b></td>
<td>
<input id="BRANDId" name="brand_id" type="text" maxlength="60" style="width:146px; border:1px solid #999999" />

</td>
</tr>
<tr>
<td><b>Brand Name*:</b></td>
<td>
<input id="BRANDName" name="bname" type="text" maxlength="60" style="width:146px; border:1px solid #999999" />

</td>
</tr><tr>


<td><b>Brand Description*:</b></td>
<td><input id="bdescription" name="bdescription" type="text"  style="width:250px; border:1px solid #999999" /></td>
</tr><tr>

<td><b>SELLER NAME*:</b></td>
<td><input id="seller_name" name="seller_name" type="text" maxlength="120" style="width:350px; border:1px solid #999999" /></td>
</tr><tr>
<td><b>SELLER ADDRESS:</b></td>
<td><input id="seller_address" name="seller_address" type="text"  style="width:350px; border:1px solid #999999" /></td>
</tr><tr>
<td><b>SELLER CONTACT:</b></td>
<td><input id="seller_contact" name="seller_contact" type="text"  style="width:350px; border:1px solid #999999" /></td>
</tr><tr>
<td><b>BRAND LOGO*:</b></td>
<td><input id="brand_logo" name="brand_logo" type="file"  style="width:300px; border:1px solid #999999" /></td>
</tr><tr>
    <td>
<input id="Submit" name="skip_Submit" type="submit" value="Submit" />
</td>
</tr>
</table>
<br />
</form>
    </div>
    </body>
</html>
