<%-- 
    Document   : Logout
    Created on : Aug 7, 2014, 6:36:35 AM
    Author     : rohitsingh
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<head>
<title>Shop Online</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<link href="style/style.css" rel="stylesheet" type="text/css" />

</head>
<body>
<center>
    <%@include file="User.jsp" %> 
 
  <div class="body-wrapper">
      <%
      request.getSession(false).invalidate();
      
      RequestDispatcher rd=request.getRequestDispatcher("login.html");
      rd.forward(request, response);
     
      %>
    <div class="body-right">
      <div class="body-right-box1">
       
        <br />
        
      </div>
    </div>
  </div>
</center>
    </body>
</html>
