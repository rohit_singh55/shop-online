<%-- 
    Document   : ViewCart
    Created on : Aug 6, 2014, 1:55:38 AM
    Author     : rohitsingh
--%>

<%@page import="java.io.FileOutputStream"%>
<%@page import="Model.SearchProduct"%>
<%@page import="java.io.InputStream"%>
<%@page import="java.sql.ResultSet"%>
<%@page import="Model.Cart"%>
<%@ page errorPage="error.jsp" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
    </head>
    <body></script>
<link href="style/style.css" rel="stylesheet" type="text/css" />
<style>
  .first-column,
  .column {
    float:left;
    width:300px;
    
  }
  .column {
    margin-left:30px;
    margin: 40px;
  }
</style>
</head>
<body>
<center>
      <div class="body-wrapper">
    <%@include file="User.jsp" %> 
    <%  Cart cart=new Cart();
    
    Integer g=(Integer)request.getSession(false).getAttribute("user_id");
    if(g==null)
        out.println("<p>g null</p>");
    else{
    ResultSet rs=cart.getCart((g+""));
    
    if(rs==null)
             out.println("rs null");
    else{
    
    %>
    <div class="body-wrapper">
        <h2><%=request.getSession(false).getAttribute("firstname") %> Your Cart
  </h2>
       <table class="table" width="400" cellpadding="10" cellspacing="5">
  <tbody>
       
    <% 
    String path=getServletContext().getRealPath("/");
    %>
       <%!Double sum=0.0; %>
      <%while(rs.next()){ 
          String ss=rs.getString(1);
          if(ss==null)
             out.println("ss null");
      %>
   
      <tr data-url="Product.jsp?pid=<%=ss%>">
          <%SearchProduct p=new SearchProduct();
          if(p==null)
             out.println("p null");
          ResultSet rbp=p.getProductbyid(ss);
         if(rbp==null)
             out.println("rbp null");
          %>
          <%while(rbp.next()){ %>
        <%
      
      
InputStream binaryStream = rbp.getBinaryStream("product_image");
int x=binaryStream.available();
  byte b[] = new byte[x]; 
  binaryStream.read(b);
FileOutputStream f1=new FileOutputStream(path+"/"+rs.getString(3)+".png");
f1.write(b);
        %>
      <td style="border:none; vertical-align:"top";"   align="top" width="20%">
          <div id="myimg"><img width="200" height="100" src="<%=rbp.getString(3)+".png" %>" style="border: 1px solid #000000; display: block;" alt="" width="294" height="300" /></div>
          <br>
          
      </td>
      <td style="border:none; vertical-align:top;" valign="top">
          <div style="font:10pt/14pt verdana;"  align="center" valign="top"><p><%=rbp.getString(3)%></p><br/></div>
          
      </td>
     
      <td style="border:none; vertical-align:top;" valign="center">
          <div style="font:12pt/16pt verdana;" align="center" valign="center"><p><span class="WebRupee">&#x20B9;</span><%=rbp.getString(6)%></p></div>
        <div style="font:10pt/14pt verdana;"  align="center" valign="top"><p>Quantity :  <%=rs.getString(3)%></p><br/></div>
      </td>
       <%
       sum+=rs.getInt(3)*rbp.getDouble(6);
       %>
    </tr>
      <br>
      
       <%}%>
      <%}}}%>
      <%
      request.getSession().setAttribute("total", sum);
      %>
      <tfoot>
    	<tr>
            <td colspan="3" class="rounded-foot-left"><h1><em>Total Sum is <%=sum %></em></h1></td>
        	<td class="rounded-foot-right">&nbsp;</td>

        </tr>
  </tbody>
  
</table>
                
                <a href="Payment.jsp">CheckOut </a>
   <%@include file="UserFooter.jsp" %>
</center>
        </div>
 
    </body>
</html>
