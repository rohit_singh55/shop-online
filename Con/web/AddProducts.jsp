
<%@page import="Model.Category"%>
<%@page import="Model.BRAND"%>
<%@page import="java.sql.ResultSet"%>
<%@page import="java.sql.PreparedStatement"%>
<%@page import="java.sql.DriverManager"%>
<%@page import="java.sql.Connection"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
       <link rel="stylesheet" type="text/css" href="style.css" />

  
</head>
<body>
<%@ include file="AdminHeader.jsp" %>

 


<div class="center_content">  
<form action="InsertProduct" method="post" enctype="multipart/form-data" >
<table border="0"  cellpadding="5" cellspacing="0" width="600">
    <CAPTION><EM>ADD PRODUCTS</EM></CAPTION>
<tr>
<td><b>Product Name*:</b></td>
<td>
<input id="ProductName" name="pname" type="text" maxlength="60" style="width:146px; border:1px solid #999999" />

</td>
</tr><tr>

<td><b>Product Id*:</b></td>
<td><input id="productID" name="pid" type="text" maxlength="60" style="width:300px; border:1px solid #999999" /></td>
</tr><tr>

<td><b>BRAND*:</b></td>
<%
          BRAND b=new BRAND();
          ResultSet rsbrand=b.getBrand();
%>
<td><select name="Brand"> 
<% try{ while(rsbrand.next()){
%>

        
        <option value="<%=rsbrand.getString(1) %>" ><%=rsbrand.getString(2) %></option> 

<%}}catch(Exception e){
out.println(e.getMessage());
}%>
</td>
</tr><tr>
    
<td><b>Category*:</b></td>
<%
       Category c=new Category();
       ResultSet rc=c.getCategory();
%>
<td><select name="Category"> 
<%
       
try{while(rc.next()){
%>

        
        <option value="<%=rc.getString(1) %>" ><%=rc.getString(2) %></option> 

<%}}catch(Exception e){
out.println(e.getMessage());
}%>
</td>
</tr><tr>
<td><b>Stock*:</b></td>
<td><input id="Stock" name="Stock" type="number" maxlength="43" style="width:250px; border:1px solid #999999" /></td>
</tr><tr>

<td><b>Product Price*:</b></td>
<td><input id="product_price" name="product_price" type="number" maxlength="120" style="width:350px; border:1px solid #999999" /></td>
</tr><tr>
<td><b>Product Description:</b></td>
<td><input id="product_description" name="product_description" type="text"  style="width:350px; border:1px solid #999999" /></td>
</tr><tr>
<td><b>Short Description:</b></td>
<td><input id="short_description" name="short_description" type="text"  style="width:350px; border:1px solid #999999" /></td>
</tr><tr>
<td><b>Product Image*:</b></td>
<td><input id="product_image" name="product_image" type="file"  style="width:300px; border:1px solid #999999" /></td>
</tr><tr>
    <td>
<input id="skip_Submit" name="skip_Submit" type="submit" value="Submit" />
</td>
</tr>
</table>
<br />
</form>
</div>
    </body>
</html>
