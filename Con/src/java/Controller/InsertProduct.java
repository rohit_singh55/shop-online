/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package Controller;

import java.io.IOException;
import java.io.InputStream;
import java.io.PrintWriter;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.Part;

/**
 *
 * @author rohitsingh
 */
@WebServlet(name = "InsertProduct", urlPatterns = {"/InsertProduct"})
@MultipartConfig(maxFileSize = 16177215) 
public class InsertProduct extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        String category_id,product_id,product_name,brand_id,stock,product_price,product_description,short_description;
        category_id=request.getParameter("Category");
        product_id=request.getParameter("pid");
        product_name=request.getParameter("pname");
        brand_id=request.getParameter("Brand");
         Part filePart = request.getPart("product_image");
         
         InputStream inputStream = null; // input stream of the upload file
         
        // obtains the upload file part in this multipart request
        
        product_price=request.getParameter("product_price");
        product_description=request.getParameter("product_description");
        short_description=request.getParameter("short_description");
        stock=request.getParameter("Stock");
       
        PrintWriter out = response.getWriter() ;
        if (filePart != null) {
            // prints out some information for debugging
            out.println(filePart.getName());
         out.println(filePart.getSize());
          out.println(filePart.getContentType());
             
            // obtains input stream of the upload file
            inputStream = filePart.getInputStream();
        }
            try{
        try {
		Class.forName("com.mysql.jdbc.Driver");
	} catch (ClassNotFoundException e) {
		out.println("Where is your MySQL JDBC Driver?");
		
		return;
	}
 
            String ins="INSERT INTO product (category_id,product_id ,product_name ,brand_id,stock ,product_price ,product_description,short_description,product_image) VALUES (?,?,?,?,?,?,?,?,?)";
            
	Connection	connection = DriverManager
		.getConnection("jdbc:mysql://localhost:3306/com","root", "root");
                   PreparedStatement preparedStatement=connection.prepareStatement(ins);
         preparedStatement.setString(1, category_id);
         preparedStatement.setString(2,product_id);
         
            if (inputStream != null) {
                // fetches input stream of the upload file for the blob column
                preparedStatement.setBlob(9, inputStream);
            }
 preparedStatement.setString(3, product_name);
         preparedStatement.setString(4,brand_id);
         preparedStatement.setString(5, stock);
          preparedStatement.setString(6, product_price);
           preparedStatement.setString(7, product_description);
            preparedStatement.setString(8, short_description);
         preparedStatement.executeUpdate();
         out.println("Inserted");
            RequestDispatcher rd=request.getRequestDispatcher("InsertedSuccessfull.jsp");
            rd.forward(request, response);
         connection.close();
        }catch(Exception e){
        out.println(e.getMessage());
        }
        }
    
    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
