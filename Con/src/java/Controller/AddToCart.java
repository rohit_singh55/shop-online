/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package Controller;

import java.io.IOException;
import java.io.PrintWriter;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import javax.jms.Session;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author rohitsingh
 */
@WebServlet(name = "AddToCart", urlPatterns = {"/AddToCart"})
public class AddToCart extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
       if(request.getSession()==null){
           RequestDispatcher rd=request.getRequestDispatcher("error.jsp");
          //request.
              rd.forward(request, response);
       }
       else{
       Integer userid=(Integer)request.getSession(false).getAttribute("user_id");
       String product_id=request.getParameter("product_id");
       int quantity=Integer.parseInt(request.getParameter("qty"));
       try (PrintWriter out = response.getWriter()) {
            try{
               // jdbc:sqlserver://u5b42g1a2g.database.windows.net:1433;database=ProjectDB;user=rohit47@u5b42g1a2g;password={your_password_here};encrypt=true;hostNameInCertificate=*.database.windows.net;loginTimeout=30;
   try {
		Class.forName("com.mysql.jdbc.Driver");
	} catch (ClassNotFoundException e) {
		out.println("Where is your MySQL JDBC Driver?");
		
		return;
	}
 
            String ins="INSERT INTO cart  VALUES (?,?,?)";
            
	Connection	connection = DriverManager
		.getConnection("jdbc:mysql://localhost:3306/com","root", "root");
                   PreparedStatement preparedStatement=connection.prepareStatement(ins);
                   
         preparedStatement.setInt(2, userid);
         preparedStatement.setString(1, product_id);
           preparedStatement.setInt(3,quantity);

       
         preparedStatement.executeUpdate();
         out.println("Insetred");
                connection.setAutoCommit(true);
 RequestDispatcher rd=request.getRequestDispatcher("ViewCart.jsp");
 rd.forward(request, response);
           }
            catch(Exception e){
        out.println("Error message: "+ e); 
            }
        
            /* TODO output your page here. You may use following sample code. */
            out.println("<!DOCTYPE html>");
            out.println("<html>");
            out.println("<head>");
            out.println("<title>Servlet Register</title>");            
            out.println("</head>");
            out.println("<body>");
           // out.println("<h1> "+firstname+"  "+lastname+" "+email+" "+country+"</h1>");
            out.println("</body>");
            out.println("</html>");
        
    }
       
       }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
