package Controller;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

import java.io.IOException;
import java.io.PrintWriter;
import java.sql.DriverManager;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.sql.*;

/**
 *
 * @author rohitsingh
 */
@WebServlet(urlPatterns = {"/Register"})
public class Register extends HttpServlet {
String firstname,lastname,email,country,pas;
   Connection con=null;
      Statement stmt = null;
      ResultSet rs=null; 

    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        firstname=request.getParameter("fname");
        lastname=request.getParameter("lname");
        email=request.getParameter("email");
        country=request.getParameter("Country");
        pas=request.getParameter("pass");
        String address1=request.getParameter("address1");
         String address2=request.getParameter("address2");
          String mobile=request.getParameter("mobile");
           String zip=request.getParameter("zip");
            String state=request.getParameter("state");
             String city=request.getParameter("city");
        try (PrintWriter out = response.getWriter()) {
            try{
               // jdbc:sqlserver://u5b42g1a2g.database.windows.net:1433;database=ProjectDB;user=rohit47@u5b42g1a2g;password={your_password_here};encrypt=true;hostNameInCertificate=*.database.windows.net;loginTimeout=30;
   try {
		Class.forName("com.mysql.jdbc.Driver");
	} catch (ClassNotFoundException e) {
		out.println("Where is your MySQL JDBC Driver?");
		
		return;
	}
 
            String ins="INSERT INTO user_details (firstname,lasttname,email,password,admin,country,address1,address2,city,state,zip,mobile) VALUES (?,?,?,?,?,?,?,?,?,?,?,?)";
            
	Connection	connection = DriverManager
		.getConnection("jdbc:mysql://localhost:3306/com","root", "root");
                   PreparedStatement preparedStatement=connection.prepareStatement(ins);
         preparedStatement.setString(1, firstname);
         preparedStatement.setString(2, lastname);
           preparedStatement.setString(3,email);
         preparedStatement.setString(4,pas);
       
         preparedStatement.setString(5, "user");
                  preparedStatement.setString(6, country);
                      preparedStatement.setString(7, address1);
                          preparedStatement.setString(8, address2);
                              preparedStatement.setString(9, city);
                                  preparedStatement.setString(10, state);
                                      preparedStatement.setString(11, zip);
                                          preparedStatement.setString(12, mobile);
         preparedStatement.executeUpdate();
         out.println("Insetred");
                connection.setAutoCommit(true);
 
         
           }
            catch(Exception e){
        out.print("Error message: "+ e.getMessage()); 
            }
        
            /* TODO output your page here. You may use following sample code. */
            out.println("<!DOCTYPE html>");
            out.println("<html>");
            out.println("<head>");
            out.println("<title>Servlet Register</title>");            
            out.println("</head>");
            out.println("<body>");
            out.println("<h1> "+firstname+"  "+lastname+" "+email+" "+country+"</h1>");
            out.println("</body>");
            out.println("</html>");
        }
    }
    
    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
