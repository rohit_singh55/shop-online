/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package Controller;

import com.oreilly.servlet.MultipartRequest;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.PrintWriter;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import javax.servlet.http.Part;
import javax.imageio.stream.FileImageInputStream;
import javax.mail.Multipart;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author rohitsingh
 */
@WebServlet(name = "ADDBRANDS", urlPatterns = {"/ADDBRANDS"})
@MultipartConfig(maxFileSize = 16177215)  
public class ADDBRANDS extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        // gets values of text fields
        String bid = request.getParameter("brand_id");
        String bname = request.getParameter("bname");
       Part filePart = request.getPart("brand_logo");
         InputStream inputStream = null; // input stream of the upload file
         
        // obtains the upload file part in this multipart request
        
         
        String bdescription = request.getParameter("bdescription");
        String sname = request.getParameter("seller_name");
        String saddress = request.getParameter("seller_address");
        String scontact = request.getParameter("seller_contact");
        String path=getServletContext().getRealPath("");
       
        PrintWriter out=response.getWriter();
        if (filePart != null) {
            // prints out some information for debugging
            out.println(filePart.getName());
         out.println(filePart.getSize());
          out.println(filePart.getContentType());
             
            // obtains input stream of the upload file
            inputStream = filePart.getInputStream();
        }
        try{
        try {
		Class.forName("com.mysql.jdbc.Driver");
	} catch (ClassNotFoundException e) {
		out.println("Where is your MySQL JDBC Driver?");
		
		return;
	}
 
            String ins="INSERT INTO brand (brand_id,brand_name ,brand_description ,SELLER,SELLER_ADDRESS ,SELLER_CONTACT ,brand_logo) VALUES (?,?,?,?,?,?,?)";
            
	Connection	connection = DriverManager
		.getConnection("jdbc:mysql://localhost:3306/com","root", "root");
                   PreparedStatement preparedStatement=connection.prepareStatement(ins);
         preparedStatement.setString(2, bname);
         preparedStatement.setString(3,bdescription);
         
            if (inputStream != null) {
                // fetches input stream of the upload file for the blob column
                preparedStatement.setBlob(7, inputStream);
            }
 preparedStatement.setString(1, bid);
         preparedStatement.setString(4,sname);
         preparedStatement.setString(5, saddress);
          preparedStatement.setString(6, scontact);
         preparedStatement.executeUpdate();
         out.println("Inserted");
            RequestDispatcher rd=request.getRequestDispatcher("InsertedSuccessfull.jsp");
            rd.forward(request, response);
         connection.close();
        }catch(Exception e){
        out.println(e.getMessage());
        }
        
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
