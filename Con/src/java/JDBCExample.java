 
import java.sql.DriverManager;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
 
public class JDBCExample {
 
  public static void main(String[] argv) {
 
	System.out.println("-------- MySQL JDBC Connection Testing ------------");
 
	try {
		Class.forName("com.mysql.jdbc.Driver");
	} catch (ClassNotFoundException e) {
		System.out.println("Where is your MySQL JDBC Driver?");
		e.printStackTrace();
		return;
	}
 
	System.out.println("MySQL JDBC Driver Registered!");
	Connection connection = null;
 
	try {
            String ins="INSERT INTO user_details (name,email,password,admin,country,address,mobile) VALUES (?,?,?,?,?,?,?)";
            
		connection = DriverManager
		.getConnection("jdbc:mysql://localhost:3306/com","root", "root");
                   PreparedStatement preparedStatement=connection.prepareStatement(ins);
         preparedStatement.setString(1, "Krishna");
         preparedStatement.setString(2, "ram@yahoo.com");
           preparedStatement.setString(3,"admin");
         preparedStatement.setString(4,"ram");
         preparedStatement.setString(5, "India");
         preparedStatement.setString(6, "Howrah");
                  preparedStatement.setString(7, "9903049966");
         preparedStatement.executeUpdate();
         System.out.println("Insetred");
                connection.setAutoCommit(true);
 
	} catch (SQLException e) {
		System.out.println(e.getMessage());
		e.printStackTrace();
		return;
	}
 
	if (connection != null) {
		System.out.println("You made it, take control your database now!");
	} else {
		System.out.println("Failed to make connection!");
	}
  }
}