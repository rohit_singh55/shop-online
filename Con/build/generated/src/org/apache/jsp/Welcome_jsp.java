package org.apache.jsp;

import javax.servlet.*;
import javax.servlet.http.*;
import javax.servlet.jsp.*;
import java.io.InputStream;
import java.io.FileOutputStream;
import Model.BRAND;
import java.sql.ResultSet;
import Model.Category;

public final class Welcome_jsp extends org.apache.jasper.runtime.HttpJspBase
    implements org.apache.jasper.runtime.JspSourceDependent {

  private static final JspFactory _jspxFactory = JspFactory.getDefaultFactory();

  private static java.util.List<String> _jspx_dependants;

  static {
    _jspx_dependants = new java.util.ArrayList<String>(2);
    _jspx_dependants.add("/User.jsp");
    _jspx_dependants.add("/UserFooter.jsp");
  }

  private org.glassfish.jsp.api.ResourceInjector _jspx_resourceInjector;

  public java.util.List<String> getDependants() {
    return _jspx_dependants;
  }

  public void _jspService(HttpServletRequest request, HttpServletResponse response)
        throws java.io.IOException, ServletException {

    PageContext pageContext = null;
    ServletContext application = null;
    ServletConfig config = null;
    JspWriter out = null;
    Object page = this;
    JspWriter _jspx_out = null;
    PageContext _jspx_page_context = null;

    try {
      response.setContentType("text/html");
      pageContext = _jspxFactory.getPageContext(this, request, response,
      			null, false, 8192, true);
      _jspx_page_context = pageContext;
      application = pageContext.getServletContext();
      config = pageContext.getServletConfig();
      out = pageContext.getOut();
      _jspx_out = out;
      _jspx_resourceInjector = (org.glassfish.jsp.api.ResourceInjector) application.getAttribute("com.sun.appserv.jsp.resource.injector");

      out.write("\n");
      out.write("\n");
      out.write("\n");
      out.write("\n");
      out.write("\n");
      out.write("<!DOCTYPE html PUBLIC \"-//W3C//DTD XHTML 1.0 Transitional//EN\" \"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd\">\n");
      out.write("<html xmlns=\"http://www.w3.org/1999/xhtml\">\n");
      out.write("<head>\n");
      out.write("<title>Shop Online</title>\n");
      out.write("<meta http-equiv=\"Content-Type\" content=\"text/html; charset=iso-8859-1\" />\n");
      out.write("<link href=\"style/style.css\" rel=\"stylesheet\" type=\"text/css\" />\n");
      out.write("\n");
      out.write("</head>\n");
      out.write("<body>\n");
      out.write("<center>\n");
      out.write("    ");
      out.write("\n");
      out.write("\n");
      out.write("\n");
      out.write("\n");
      out.write("<!DOCTYPE html>\n");
      out.write("<html>\n");
      out.write("    <head>\n");
      out.write("        <meta http-equiv=\"Content-Type\" content=\"text/html; charset=UTF-8\">\n");
      out.write("        <title>Shop Online</title>\n");
      out.write("    \n");
      out.write("<meta http-equiv=\"Content-Type\" content=\"text/html; charset=iso-8859-1\" />\n");
      out.write("<link href=\"style/style.css\" rel=\"stylesheet\" type=\"text/css\" />\n");
      out.write("\n");
      out.write("</head>\n");
      out.write("<body>\n");

HttpSession gs=request.getSession(false);
if(gs==null){
RequestDispatcher rd=request.getRequestDispatcher("login.html");
rd.forward(request, response);
}

      out.write("\n");
      out.write("<center>\n");
      out.write("    <div class=\"header\">\n");
      out.write("    <div class=\"logo\">Shop<strong>Online</strong></div>\n");
      out.write("    \n");
      out.write("   \n");
      out.write("  \n");
      out.write("    <div class=\"menuu\">\n");
      out.write("        <div class=\"right_header\">Welcome  ");
 out.println(request.getSession(false).getAttribute("firstname")); 
      out.write(" <a href=\"ViewCart.jsp\">CART</a> | | <a href=\"Payment.jsp\" >Checkout</a> || <a href=\"Logout.jsp\" class=\"logout\">Logout</a></div>\n");
      out.write("   \n");
      out.write("    </div>\n");
      out.write("    </div>\n");
      out.write("         <div class=\"menu\">\n");
      out.write("      <ul class=\"solidblockmenu\">\n");
      out.write("        <li><a href=\"Welcome.jsp\">Home</a></li>\n");
      out.write("        <li><a href=\"UserCategories.jsp\">Categories</a></li>\n");
      out.write("        <li><a href=\"UserGuide.jsp\">User Guide</a></li>\n");
      out.write("        <li><a href=\"Review.jsp\">Reviews</a></li>\n");
      out.write("        <li><a href=\"HotDeals.jsp\">Hot Deals</a></li>\n");
      out.write("        <li><a href=\"FAQ.jsp\">FAQ's</a></li>\n");
      out.write("      </ul>\n");
      out.write("      <div class=\"clear\"></div>\n");
      out.write("    </div>\n");
      out.write("  </div>\n");
      out.write("     <div class=\"search\">\n");
      out.write("      <form action=\"UserSearch.jsp\" method=\"get\">\n");
      out.write("    <div class=\"search-text\"> Search the world of shopping!&nbsp;\n");
      out.write("        <input type=\"text\" size=\"40\" name=\"srch\" />\n");
      out.write("      &nbsp;&nbsp;</div>\n");
      out.write("    <div style=\"float:left; margin-left:5px; margin-top:10px;\">\n");
      out.write("      <input type=\"submit\" src=\"images/search.jpg\" />\n");
      out.write("    </div>\n");
      out.write("      </form>\n");
      out.write("  </div>\n");
      out.write("  <div class=\"hot-search\">\n");
      out.write("    <div class=\"hot-search-text\"> Hot Searches: Apple iPod Nano, Hugo Boss Clothing, HD DVD Player, DSLR Camera, Toshiba 32&quot; LCD TV, Unsecured Loans, Excersise Machines..</div>\n");
      out.write("  </div>\n");
      out.write("    </body>\n");
      out.write("</html>\n");
      out.write(" \n");
      out.write(" \n");
      out.write("  <div class=\"body-wrapper\">\n");
      out.write("      <TABLE WIDTH=780 align=center cellspacing=0 cellpadding=4 style=\"font:bold 11px verdana\">\n");
      out.write("\n");
      out.write("<TR bgcolor=#234567><TH valign=top width=150>Offer</TH><TH width=400><marquee direction=right>");
      out.print("Prices Slashed");
      out.write("&nbsp;&nbsp;|&nbsp;&nbsp;<a href=\"");
      out.print("/Welcome.jsp");
      out.write("\" class=caption>Home</a></TH></TR></TABLE>\n");
      out.write("    <div class=\"body-right\">\n");
      out.write("      <div class=\"body-right-box1\">\n");
      out.write("       \n");
      out.write("        <br />\n");
      out.write("        \n");
      out.write("      </div>\n");
      out.write("      <div class=\"separator\"> &nbsp;</div>\n");
      out.write("      <div class=\"body-right-ad1\"> &nbsp;</div>\n");
      out.write("      <div class=\"separator\"> &nbsp;</div>\n");
      out.write("      <div class=\"body-right-ad2\"> &nbsp;</div>\n");
      out.write("      <div class=\"separator\"> &nbsp;</div>\n");
      out.write("      <div class=\"body-right-ad3\"> &nbsp;</div>\n");
      out.write("    </div>\n");
      out.write("    <div class=\"divider1\"> &nbsp;</div>\n");
      out.write("  \n");
      out.write("    <div class=\"divider2\"> &nbsp;</div>\n");
      out.write("  \n");
      out.write("   <div id=\"content\">\n");
      out.write("    <div id=\"content_left\">\n");
      out.write("      <div class=\"content_left_section\">\n");
      out.write("          ");

          Category c=new Category();
          ResultSet rc=c.getCategory();
          
      out.write("\n");
      out.write("        <h1>Categories</h1>\n");
      out.write("        <ul>\n");
      out.write("            ");
 while(rc.next()){ 
      out.write("\n");
      out.write("            <li><a href=\"CategoryProducts.jsp?id=");
      out.print(rc.getString(1) );
      out.write('"');
      out.write('>');
      out.print(rc.getString(2) );
      out.write("</a></li>\n");
      out.write("          ");
}
      out.write("\n");
      out.write("        </ul>\n");
      out.write("      </div>\n");
      out.write("        <div class=\"content_left_section\">\n");
      out.write("        <h1>Brands</h1>\n");
      out.write("        ");

                String path=getServletContext().getRealPath("/");
        BRAND br=new BRAND();
        ResultSet rbr=br.getBrand();
        
        
      out.write("\n");
      out.write("        <ul>\n");
      out.write("             ");
 while(rbr.next()){ 
      out.write("\n");
      out.write("          <li>");

      
      
InputStream brbinaryStream = rbr.getBinaryStream("brand_logo");
int brx=brbinaryStream.available();
  byte bb[] = new byte[brx]; 
  brbinaryStream.read(bb);
FileOutputStream brf1=new FileOutputStream(path+"/"+rbr.getString(1)+".png");
brf1.write(bb);
        
      out.write("\n");
      out.write("        \n");
      out.write("        \n");
      out.write("    <div ><p>");
      out.print(rbr.getString(2) );
      out.write("<img width=\"25\" align=\"middle\" height=\"30\"  src=\"");
      out.print(rbr.getString(1)+".png" );
      out.write("\" /></p></div></li>\n");
      out.write("           ");
}
      out.write("\n");
      out.write("        </ul>\n");
      out.write("      </div>\n");
      out.write("    </div>\n");
      out.write("   </div>\n");
      out.write("    </div>\n");
      out.write("</div>\n");
      out.write("    ");
      out.write("\n");
      out.write("\n");
      out.write("\n");
      out.write("<!DOCTYPE html>\n");
      out.write("<html>\n");
      out.write("    <head>\n");
      out.write("        <meta http-equiv=\"Content-Type\" content=\"text/html; charset=UTF-8\">\n");
      out.write("        <title>JSP Page</title>\n");
      out.write("    </head>\n");
      out.write("    <body>\n");
      out.write("       <div class=\"separator-main\"> &nbsp;</div>\n");
      out.write("  <div class=\"footer\">\n");
      out.write("    <div class=\"footer-text\" style=\"padding-top:10px;margin-left:20px;\"> <a href=\"http://all-free-download.com/free-website-templates/\" class=\"nav1\">Home</a>&nbsp;&nbsp;<span style=\"color:#dbdbdb\">|</span>&nbsp;&nbsp;<a href=\"http://all-free-download.com/free-website-templates/\" class=\"nav1\">Categories</a>&nbsp;&nbsp;<span style=\"color:#dbdbdb\">|</span>&nbsp;&nbsp;<a href=\"http://all-free-download.com/free-website-templates/\" class=\"nav1\">User Guide</a>&nbsp;&nbsp;<span style=\"color:#dbdbdb\">|</span>&nbsp;&nbsp;<a href=\"http://all-free-download.com/free-website-templates/\" class=\"nav1\">Reviews</a>&nbsp;&nbsp;<span style=\"color:#dbdbdb\">|</span>&nbsp;&nbsp;<a href=\"http://all-free-download.com/free-website-templates/\" class=\"nav1\">Hot Deals</a>&nbsp;&nbsp;<span style=\"color:#dbdbdb\">|</span>&nbsp;&nbsp;<a href=\"http://all-free-download.com/free-website-templates/\" class=\"nav1\">FAQ&#8217;s</a>&nbsp;&nbsp;<span style=\"color:#dbdbdb\">|</span>&nbsp;&nbsp;<a href=\"http://all-free-download.com/free-website-templates/\" class=\"nav1\">Contact</a>&nbsp;&nbsp;<span style=\"color:#dbdbdb\">|</span>&nbsp;&nbsp;<a href=\"http://all-free-download.com/free-website-templates/\" class=\"nav1\">Privacy Policy</a></div>\n");
      out.write("    <div class=\"footer-text\" style=\"padding-top:20px;margin-left:20px;\"> Developed by Rohit Singh  </div>\n");
      out.write("      \n");
      out.write("    </div>\n");
      out.write("  </div>\n");
      out.write("    </body>\n");
      out.write("</html>\n");
      out.write("\n");
      out.write("</center>\n");
      out.write("</body>\n");
      out.write("</html>\n");
    } catch (Throwable t) {
      if (!(t instanceof SkipPageException)){
        out = _jspx_out;
        if (out != null && out.getBufferSize() != 0)
          out.clearBuffer();
        if (_jspx_page_context != null) _jspx_page_context.handlePageException(t);
        else throw new ServletException(t);
      }
    } finally {
      _jspxFactory.releasePageContext(_jspx_page_context);
    }
  }
}
