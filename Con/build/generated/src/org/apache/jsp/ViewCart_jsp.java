package org.apache.jsp;

import javax.servlet.*;
import javax.servlet.http.*;
import javax.servlet.jsp.*;
import java.io.FileOutputStream;
import Model.SearchProduct;
import java.io.InputStream;
import java.sql.ResultSet;
import Model.Cart;

public final class ViewCart_jsp extends org.apache.jasper.runtime.HttpJspBase
    implements org.apache.jasper.runtime.JspSourceDependent {

Double sum=0.0; 
  private static final JspFactory _jspxFactory = JspFactory.getDefaultFactory();

  private static java.util.List<String> _jspx_dependants;

  static {
    _jspx_dependants = new java.util.ArrayList<String>(2);
    _jspx_dependants.add("/User.jsp");
    _jspx_dependants.add("/UserFooter.jsp");
  }

  private org.glassfish.jsp.api.ResourceInjector _jspx_resourceInjector;

  public java.util.List<String> getDependants() {
    return _jspx_dependants;
  }

  public void _jspService(HttpServletRequest request, HttpServletResponse response)
        throws java.io.IOException, ServletException {

    PageContext pageContext = null;
    ServletContext application = null;
    ServletConfig config = null;
    JspWriter out = null;
    Object page = this;
    JspWriter _jspx_out = null;
    PageContext _jspx_page_context = null;

    try {
      response.setContentType("text/html;charset=UTF-8");
      pageContext = _jspxFactory.getPageContext(this, request, response,
      			"error.jsp", false, 8192, true);
      _jspx_page_context = pageContext;
      application = pageContext.getServletContext();
      config = pageContext.getServletConfig();
      out = pageContext.getOut();
      _jspx_out = out;
      _jspx_resourceInjector = (org.glassfish.jsp.api.ResourceInjector) application.getAttribute("com.sun.appserv.jsp.resource.injector");

      out.write("\n");
      out.write("\n");
      out.write("\n");
      out.write("\n");
      out.write("\n");
      out.write("\n");
      out.write("\n");
      out.write("\n");
      out.write("\n");
      out.write("<!DOCTYPE html>\n");
      out.write("<html>\n");
      out.write("    <head>\n");
      out.write("        <meta http-equiv=\"Content-Type\" content=\"text/html; charset=UTF-8\">\n");
      out.write("        <title>JSP Page</title>\n");
      out.write("    </head>\n");
      out.write("    <body></script>\n");
      out.write("<link href=\"style/style.css\" rel=\"stylesheet\" type=\"text/css\" />\n");
      out.write("<style>\n");
      out.write("  .first-column,\n");
      out.write("  .column {\n");
      out.write("    float:left;\n");
      out.write("    width:300px;\n");
      out.write("    \n");
      out.write("  }\n");
      out.write("  .column {\n");
      out.write("    margin-left:30px;\n");
      out.write("    margin: 40px;\n");
      out.write("  }\n");
      out.write("</style>\n");
      out.write("</head>\n");
      out.write("<body>\n");
      out.write("<center>\n");
      out.write("      <div class=\"body-wrapper\">\n");
      out.write("    ");
      out.write("\n");
      out.write("\n");
      out.write("\n");
      out.write("\n");
      out.write("<!DOCTYPE html>\n");
      out.write("<html>\n");
      out.write("    <head>\n");
      out.write("        <meta http-equiv=\"Content-Type\" content=\"text/html; charset=UTF-8\">\n");
      out.write("        <title>Shop Online</title>\n");
      out.write("    \n");
      out.write("<meta http-equiv=\"Content-Type\" content=\"text/html; charset=iso-8859-1\" />\n");
      out.write("<link href=\"style/style.css\" rel=\"stylesheet\" type=\"text/css\" />\n");
      out.write("\n");
      out.write("</head>\n");
      out.write("<body>\n");

HttpSession gs=request.getSession(false);
if(gs==null){
RequestDispatcher rd=request.getRequestDispatcher("login.html");
rd.forward(request, response);
}

      out.write("\n");
      out.write("<center>\n");
      out.write("    <div class=\"header\">\n");
      out.write("    <div class=\"logo\">Shop<strong>Online</strong></div>\n");
      out.write("    \n");
      out.write("   \n");
      out.write("  \n");
      out.write("    <div class=\"menuu\">\n");
      out.write("        <div class=\"right_header\">Welcome  ");
 out.println(request.getSession(false).getAttribute("firstname")); 
      out.write(" <a href=\"ViewCart.jsp\">CART</a> | | <a href=\"Payment.jsp\" >Checkout</a> || <a href=\"Logout.jsp\" class=\"logout\">Logout</a></div>\n");
      out.write("   \n");
      out.write("    </div>\n");
      out.write("    </div>\n");
      out.write("         <div class=\"menu\">\n");
      out.write("      <ul class=\"solidblockmenu\">\n");
      out.write("        <li><a href=\"Welcome.jsp\">Home</a></li>\n");
      out.write("        <li><a href=\"UserCategories.jsp\">Categories</a></li>\n");
      out.write("        <li><a href=\"UserGuide.jsp\">User Guide</a></li>\n");
      out.write("        <li><a href=\"Review.jsp\">Reviews</a></li>\n");
      out.write("        <li><a href=\"HotDeals.jsp\">Hot Deals</a></li>\n");
      out.write("        <li><a href=\"FAQ.jsp\">FAQ's</a></li>\n");
      out.write("      </ul>\n");
      out.write("      <div class=\"clear\"></div>\n");
      out.write("    </div>\n");
      out.write("  </div>\n");
      out.write("     <div class=\"search\">\n");
      out.write("      <form action=\"UserSearch.jsp\" method=\"get\">\n");
      out.write("    <div class=\"search-text\"> Search the world of shopping!&nbsp;\n");
      out.write("        <input type=\"text\" size=\"40\" name=\"srch\" />\n");
      out.write("      &nbsp;&nbsp;</div>\n");
      out.write("    <div style=\"float:left; margin-left:5px; margin-top:10px;\">\n");
      out.write("      <input type=\"submit\" src=\"images/search.jpg\" />\n");
      out.write("    </div>\n");
      out.write("      </form>\n");
      out.write("  </div>\n");
      out.write("  <div class=\"hot-search\">\n");
      out.write("    <div class=\"hot-search-text\"> Hot Searches: Apple iPod Nano, Hugo Boss Clothing, HD DVD Player, DSLR Camera, Toshiba 32&quot; LCD TV, Unsecured Loans, Excersise Machines..</div>\n");
      out.write("  </div>\n");
      out.write("    </body>\n");
      out.write("</html>\n");
      out.write(" \n");
      out.write("    ");
  Cart cart=new Cart();
    
    Integer g=(Integer)request.getSession(false).getAttribute("user_id");
    if(g==null)
        out.println("<p>g null</p>");
    else{
    ResultSet rs=cart.getCart((g+""));
    
    if(rs==null)
             out.println("rs null");
    else{
    
    
      out.write("\n");
      out.write("    <div class=\"body-wrapper\">\n");
      out.write("        <h2>");
      out.print(request.getSession(false).getAttribute("firstname") );
      out.write(" Your Cart\n");
      out.write("  </h2>\n");
      out.write("       <table class=\"table\" width=\"400\" cellpadding=\"10\" cellspacing=\"5\">\n");
      out.write("  <tbody>\n");
      out.write("       \n");
      out.write("    ");
 
    String path=getServletContext().getRealPath("/");
    
      out.write("\n");
      out.write("       ");
      out.write("\n");
      out.write("      ");
while(rs.next()){ 
          String ss=rs.getString(1);
          if(ss==null)
             out.println("ss null");
      
      out.write("\n");
      out.write("   \n");
      out.write("      <tr data-url=\"Product.jsp?pid=");
      out.print(ss);
      out.write("\">\n");
      out.write("          ");
SearchProduct p=new SearchProduct();
          if(p==null)
             out.println("p null");
          ResultSet rbp=p.getProductbyid(ss);
         if(rbp==null)
             out.println("rbp null");
          
      out.write("\n");
      out.write("          ");
while(rbp.next()){ 
      out.write("\n");
      out.write("        ");

      
      
InputStream binaryStream = rbp.getBinaryStream("product_image");
int x=binaryStream.available();
  byte b[] = new byte[x]; 
  binaryStream.read(b);
FileOutputStream f1=new FileOutputStream(path+"/"+rs.getString(3)+".png");
f1.write(b);
        
      out.write("\n");
      out.write("      <td style=\"border:none; vertical-align:\"top\";\"   align=\"top\" width=\"20%\">\n");
      out.write("          <div id=\"myimg\"><img width=\"200\" height=\"100\" src=\"");
      out.print(rbp.getString(3)+".png" );
      out.write("\" style=\"border: 1px solid #000000; display: block;\" alt=\"\" width=\"294\" height=\"300\" /></div>\n");
      out.write("          <br>\n");
      out.write("          \n");
      out.write("      </td>\n");
      out.write("      <td style=\"border:none; vertical-align:top;\" valign=\"top\">\n");
      out.write("          <div style=\"font:10pt/14pt verdana;\"  align=\"center\" valign=\"top\"><p>");
      out.print(rbp.getString(3));
      out.write("</p><br/></div>\n");
      out.write("          \n");
      out.write("      </td>\n");
      out.write("     \n");
      out.write("      <td style=\"border:none; vertical-align:top;\" valign=\"center\">\n");
      out.write("          <div style=\"font:12pt/16pt verdana;\" align=\"center\" valign=\"center\"><p><span class=\"WebRupee\">&#x20B9;</span>");
      out.print(rbp.getString(6));
      out.write("</p></div>\n");
      out.write("        <div style=\"font:10pt/14pt verdana;\"  align=\"center\" valign=\"top\"><p>Quantity :  ");
      out.print(rs.getString(3));
      out.write("</p><br/></div>\n");
      out.write("      </td>\n");
      out.write("       ");

       sum+=rs.getInt(3)*rbp.getDouble(6);
       
      out.write("\n");
      out.write("    </tr>\n");
      out.write("      <br>\n");
      out.write("      \n");
      out.write("       ");
}
      out.write("\n");
      out.write("      ");
}}}
      out.write("\n");
      out.write("      ");

      request.getSession().setAttribute("total", sum);
      
      out.write("\n");
      out.write("      <tfoot>\n");
      out.write("    \t<tr>\n");
      out.write("            <td colspan=\"3\" class=\"rounded-foot-left\"><h1><em>Total Sum is ");
      out.print(sum );
      out.write("</em></h1></td>\n");
      out.write("        \t<td class=\"rounded-foot-right\">&nbsp;</td>\n");
      out.write("\n");
      out.write("        </tr>\n");
      out.write("  </tbody>\n");
      out.write("  \n");
      out.write("</table>\n");
      out.write("                \n");
      out.write("                <a href=\"Payment.jsp\">CheckOut </a>\n");
      out.write("   ");
      out.write("\n");
      out.write("\n");
      out.write("\n");
      out.write("<!DOCTYPE html>\n");
      out.write("<html>\n");
      out.write("    <head>\n");
      out.write("        <meta http-equiv=\"Content-Type\" content=\"text/html; charset=UTF-8\">\n");
      out.write("        <title>JSP Page</title>\n");
      out.write("    </head>\n");
      out.write("    <body>\n");
      out.write("       <div class=\"separator-main\"> &nbsp;</div>\n");
      out.write("  <div class=\"footer\">\n");
      out.write("    <div class=\"footer-text\" style=\"padding-top:10px;margin-left:20px;\"> <a href=\"http://all-free-download.com/free-website-templates/\" class=\"nav1\">Home</a>&nbsp;&nbsp;<span style=\"color:#dbdbdb\">|</span>&nbsp;&nbsp;<a href=\"http://all-free-download.com/free-website-templates/\" class=\"nav1\">Categories</a>&nbsp;&nbsp;<span style=\"color:#dbdbdb\">|</span>&nbsp;&nbsp;<a href=\"http://all-free-download.com/free-website-templates/\" class=\"nav1\">User Guide</a>&nbsp;&nbsp;<span style=\"color:#dbdbdb\">|</span>&nbsp;&nbsp;<a href=\"http://all-free-download.com/free-website-templates/\" class=\"nav1\">Reviews</a>&nbsp;&nbsp;<span style=\"color:#dbdbdb\">|</span>&nbsp;&nbsp;<a href=\"http://all-free-download.com/free-website-templates/\" class=\"nav1\">Hot Deals</a>&nbsp;&nbsp;<span style=\"color:#dbdbdb\">|</span>&nbsp;&nbsp;<a href=\"http://all-free-download.com/free-website-templates/\" class=\"nav1\">FAQ&#8217;s</a>&nbsp;&nbsp;<span style=\"color:#dbdbdb\">|</span>&nbsp;&nbsp;<a href=\"http://all-free-download.com/free-website-templates/\" class=\"nav1\">Contact</a>&nbsp;&nbsp;<span style=\"color:#dbdbdb\">|</span>&nbsp;&nbsp;<a href=\"http://all-free-download.com/free-website-templates/\" class=\"nav1\">Privacy Policy</a></div>\n");
      out.write("    <div class=\"footer-text\" style=\"padding-top:20px;margin-left:20px;\"> Developed by Rohit Singh  </div>\n");
      out.write("      \n");
      out.write("    </div>\n");
      out.write("  </div>\n");
      out.write("    </body>\n");
      out.write("</html>\n");
      out.write("\n");
      out.write("</center>\n");
      out.write("        </div>\n");
      out.write(" \n");
      out.write("    </body>\n");
      out.write("</html>\n");
    } catch (Throwable t) {
      if (!(t instanceof SkipPageException)){
        out = _jspx_out;
        if (out != null && out.getBufferSize() != 0)
          out.clearBuffer();
        if (_jspx_page_context != null) _jspx_page_context.handlePageException(t);
        else throw new ServletException(t);
      }
    } finally {
      _jspxFactory.releasePageContext(_jspx_page_context);
    }
  }
}
