package org.apache.jsp;

import javax.servlet.*;
import javax.servlet.http.*;
import javax.servlet.jsp.*;

public final class OrderConfirmation_jsp extends org.apache.jasper.runtime.HttpJspBase
    implements org.apache.jasper.runtime.JspSourceDependent {

  private static final JspFactory _jspxFactory = JspFactory.getDefaultFactory();

  private static java.util.List<String> _jspx_dependants;

  static {
    _jspx_dependants = new java.util.ArrayList<String>(1);
    _jspx_dependants.add("/User.jsp");
  }

  private org.glassfish.jsp.api.ResourceInjector _jspx_resourceInjector;

  public java.util.List<String> getDependants() {
    return _jspx_dependants;
  }

  public void _jspService(HttpServletRequest request, HttpServletResponse response)
        throws java.io.IOException, ServletException {

    PageContext pageContext = null;
    ServletContext application = null;
    ServletConfig config = null;
    JspWriter out = null;
    Object page = this;
    JspWriter _jspx_out = null;
    PageContext _jspx_page_context = null;

    try {
      response.setContentType("text/html;charset=UTF-8");
      pageContext = _jspxFactory.getPageContext(this, request, response,
      			null, false, 8192, true);
      _jspx_page_context = pageContext;
      application = pageContext.getServletContext();
      config = pageContext.getServletConfig();
      out = pageContext.getOut();
      _jspx_out = out;
      _jspx_resourceInjector = (org.glassfish.jsp.api.ResourceInjector) application.getAttribute("com.sun.appserv.jsp.resource.injector");

      out.write("\n");
      out.write("\n");
      out.write("\n");
      out.write("<!DOCTYPE html>\n");
      out.write("<html>\n");
      out.write("    <head>\n");
      out.write("        <meta http-equiv=\"Content-Type\" content=\"text/html; charset=UTF-8\">\n");
      out.write("           <meta http-equiv=\"Content-Type\" content=\"text/html; charset=UTF-8\">\n");
      out.write("       \n");
      out.write("        <style type=\"text/css\">\n");
      out.write("            #big_wrapper{\n");
      out.write("                text-align: center;\n");
      out.write("                background-color: sienna;\n");
      out.write("                color:white;\n");
      out.write("            }\n");
      out.write("            #link.active{\n");
      out.write("                color:white;\n");
      out.write("            }\n");
      out.write("        </style>\n");
      out.write("    <title>Shop Online</title>\n");
      out.write("<script src=\"http://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js\"></script>\n");
      out.write("<meta http-equiv=\"Content-Type\" content=\"text/html; charset=iso-8859-1\" />\n");
      out.write("    <link rel=\"stylesheet\" type=\"text/css\" href=\"http://cdn.webrupee.com/font\">\n");
      out.write("        <style>\n");
      out.write(" \n");
      out.write("        </style>\n");
      out.write("    <script src=http://cdn.webrupee.com/js type=”text/javascript”></script>\n");
      out.write("    \n");
      out.write("    <script type=\"text/javascript\">\n");
      out.write("        \n");
      out.write("        $(function () {\n");
      out.write("  \n");
      out.write("    </script>\n");
      out.write("        <script>\n");
      out.write("$(document).ready(function(){\n");
      out.write("  \n");
      out.write("    $(\"#myimg\").fadeIn();\n");
      out.write("    $(\"#div2\").fadeIn(\"slow\");\n");
      out.write("    $(\"#div3\").fadeIn(10000);\n");
      out.write("    $(\"#flip\").click(function(){\n");
      out.write("    $(\"#panel\").slideDown(\"slow\");\n");
      out.write("  });\n");
      out.write("  \n");
      out.write("});\n");
      out.write("</script>\n");
      out.write("<link href=\"style/style.css\" rel=\"stylesheet\" type=\"text/css\" />\n");
      out.write("<style>\n");
      out.write("  .first-column,\n");
      out.write("  .column {\n");
      out.write("    float:left;\n");
      out.write("    width:300px;\n");
      out.write("    \n");
      out.write("  }\n");
      out.write("  .column {\n");
      out.write("    margin-left:30px;\n");
      out.write("    margin: 40px;\n");
      out.write("  }\n");
      out.write("</style>\n");
      out.write("</head>\n");
      out.write("<body>\n");
      out.write("<center>\n");
      out.write("    ");
      out.write("\n");
      out.write("\n");
      out.write("\n");
      out.write("\n");
      out.write("<!DOCTYPE html>\n");
      out.write("<html>\n");
      out.write("    <head>\n");
      out.write("        <meta http-equiv=\"Content-Type\" content=\"text/html; charset=UTF-8\">\n");
      out.write("        <title>Shop Online</title>\n");
      out.write("    \n");
      out.write("<meta http-equiv=\"Content-Type\" content=\"text/html; charset=iso-8859-1\" />\n");
      out.write("<link href=\"style/style.css\" rel=\"stylesheet\" type=\"text/css\" />\n");
      out.write("\n");
      out.write("</head>\n");
      out.write("<body>\n");

HttpSession gs=request.getSession(false);
if(gs==null){
RequestDispatcher rd=request.getRequestDispatcher("login.html");
rd.forward(request, response);
}

      out.write("\n");
      out.write("<center>\n");
      out.write("    <div class=\"header\">\n");
      out.write("    <div class=\"logo\">Shop<strong>Online</strong></div>\n");
      out.write("    \n");
      out.write("   \n");
      out.write("  \n");
      out.write("    <div class=\"menuu\">\n");
      out.write("        <div class=\"right_header\">Welcome  ");
 out.println(request.getSession(false).getAttribute("firstname")); 
      out.write(" <a href=\"ViewCart.jsp\">CART</a> | | <a href=\"Payment.jsp\" >Checkout</a> || <a href=\"Logout.jsp\" class=\"logout\">Logout</a></div>\n");
      out.write("   \n");
      out.write("    </div>\n");
      out.write("    </div>\n");
      out.write("         <div class=\"menu\">\n");
      out.write("      <ul class=\"solidblockmenu\">\n");
      out.write("        <li><a href=\"Welcome.jsp\">Home</a></li>\n");
      out.write("        <li><a href=\"UserCategories.jsp\">Categories</a></li>\n");
      out.write("        <li><a href=\"UserGuide.jsp\">User Guide</a></li>\n");
      out.write("        <li><a href=\"Review.jsp\">Reviews</a></li>\n");
      out.write("        <li><a href=\"HotDeals.jsp\">Hot Deals</a></li>\n");
      out.write("        <li><a href=\"FAQ.jsp\">FAQ's</a></li>\n");
      out.write("      </ul>\n");
      out.write("      <div class=\"clear\"></div>\n");
      out.write("    </div>\n");
      out.write("  </div>\n");
      out.write("     <div class=\"search\">\n");
      out.write("      <form action=\"UserSearch.jsp\" method=\"get\">\n");
      out.write("    <div class=\"search-text\"> Search the world of shopping!&nbsp;\n");
      out.write("        <input type=\"text\" size=\"40\" name=\"srch\" />\n");
      out.write("      &nbsp;&nbsp;</div>\n");
      out.write("    <div style=\"float:left; margin-left:5px; margin-top:10px;\">\n");
      out.write("      <input type=\"submit\" src=\"images/search.jpg\" />\n");
      out.write("    </div>\n");
      out.write("      </form>\n");
      out.write("  </div>\n");
      out.write("  <div class=\"hot-search\">\n");
      out.write("    <div class=\"hot-search-text\"> Hot Searches: Apple iPod Nano, Hugo Boss Clothing, HD DVD Player, DSLR Camera, Toshiba 32&quot; LCD TV, Unsecured Loans, Excersise Machines..</div>\n");
      out.write("  </div>\n");
      out.write("    </body>\n");
      out.write("</html>\n");
      out.write(" \n");
      out.write("    \n");
      out.write(" \n");
      out.write("     <div class=\"body-wrapper\">   \n");
      out.write("         <h1>Total Cost <span class=\"WebRupee\">&#x20B9;</span>");
      out.print(request.getSession().getAttribute("total") );
      out.write("</h1>\n");
      out.write("        \n");
      out.write("        <div id=\"big_wrapper\">\n");
      out.write("       <h1> Order Has been submitted!! Look out for your product!!!  <br /> <br /> <br /> \n");
      out.write("                    Thank you for ordering at Shop Online </h1>  <br /> <br /> <br /> <br /> <br />\n");
      out.write("                <div id=\"link\">    <h2> <a href=\"Welcome.jsp\"> Shop Again! </a> </h2> </div>\n");
      out.write("   \n");
      out.write("        \n");
      out.write("        </div>\n");
      out.write("     </div></center>\n");
      out.write("        </body>\n");
      out.write("    \n");
      out.write("</html>\n");
    } catch (Throwable t) {
      if (!(t instanceof SkipPageException)){
        out = _jspx_out;
        if (out != null && out.getBufferSize() != 0)
          out.clearBuffer();
        if (_jspx_page_context != null) _jspx_page_context.handlePageException(t);
        else throw new ServletException(t);
      }
    } finally {
      _jspxFactory.releasePageContext(_jspx_page_context);
    }
  }
}
