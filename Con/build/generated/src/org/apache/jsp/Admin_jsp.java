package org.apache.jsp;

import javax.servlet.*;
import javax.servlet.http.*;
import javax.servlet.jsp.*;
import java.util.ArrayList;
import Model.Category;

public final class Admin_jsp extends org.apache.jasper.runtime.HttpJspBase
    implements org.apache.jasper.runtime.JspSourceDependent {

  private static final JspFactory _jspxFactory = JspFactory.getDefaultFactory();

  private static java.util.List<String> _jspx_dependants;

  static {
    _jspx_dependants = new java.util.ArrayList<String>(2);
    _jspx_dependants.add("/AdminHeader.jsp");
    _jspx_dependants.add("/AdminFoter.jsp");
  }

  private org.glassfish.jsp.api.ResourceInjector _jspx_resourceInjector;

  public java.util.List<String> getDependants() {
    return _jspx_dependants;
  }

  public void _jspService(HttpServletRequest request, HttpServletResponse response)
        throws java.io.IOException, ServletException {

    PageContext pageContext = null;
    HttpSession session = null;
    ServletContext application = null;
    ServletConfig config = null;
    JspWriter out = null;
    Object page = this;
    JspWriter _jspx_out = null;
    PageContext _jspx_page_context = null;

    try {
      response.setContentType("text/html;charset=UTF-8");
      pageContext = _jspxFactory.getPageContext(this, request, response,
      			null, true, 8192, true);
      _jspx_page_context = pageContext;
      application = pageContext.getServletContext();
      config = pageContext.getServletConfig();
      session = pageContext.getSession();
      out = pageContext.getOut();
      _jspx_out = out;
      _jspx_resourceInjector = (org.glassfish.jsp.api.ResourceInjector) application.getAttribute("com.sun.appserv.jsp.resource.injector");

      out.write("\n");
      out.write("\n");
      out.write("\n");
      out.write("\n");
      out.write("\n");
      out.write("<!DOCTYPE html PUBLIC \"-//W3C//DTD XHTML 1.0 Transitional//EN\" \"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd\">\n");
      out.write("<html xmlns=\"http://www.w3.org/1999/xhtml\">\n");
      out.write("<head>\n");
      out.write("<meta http-equiv=\"Content-Type\" content=\"text/html; charset=utf-8\" />\n");
      out.write("<title> ADMIN PANEL </title>\n");
      out.write("<link rel=\"stylesheet\" type=\"text/css\" href=\"style.css\" />\n");

HttpSession s=request.getSession();
if(s.isNew()){
RequestDispatcher rd=request.getRequestDispatcher("login.html");
rd.forward(request, response);
}

      out.write("\n");
      out.write("\n");
      out.write("\n");
      out.write("</head>\n");
      out.write("<body>\n");
      out.write("\n");
      out.write("\n");
      out.write("\n");
      out.write("<!DOCTYPE html>\n");
      out.write("<html>\n");
      out.write("    <head>\n");
      out.write("        <meta http-equiv=\"Content-Type\" content=\"text/html; charset=UTF-8\">\n");
      out.write("        <title>JSP Page</title>\n");
      out.write("    </head>\n");
      out.write("    <body>\n");
      out.write("        <div id=\"main_container\">\n");
      out.write("\n");
      out.write("\t<div class=\"header\">\n");
      out.write("    <div class=\"logo\"><a href=\"#\"><img src=\"images/logo.gif\" alt=\"\" title=\"\" border=\"0\" /></a></div>\n");
      out.write("    \n");
      out.write("    <div class=\"right_header\">Welcome Admin ");
 out.println(request.getSession(false).getAttribute("firstname")); 
      out.write(" <a href=\"Welcome\">Visit site</a> | | <a href=\"#\" class=\"logout\">Logout</a></div>\n");
      out.write("   \n");
      out.write("    </div>\n");
      out.write("     <div class=\"main_content\">\n");
      out.write("    \n");
      out.write("                    <div class=\"menu\">\n");
      out.write("                    <ul>\n");
      out.write("                    <li><a class=\"current\" href=\"Admin.jsp\">Admin Home</a></li>\n");
      out.write("                    <li><a href=\"Category.jsp\">Manage Categories<!--[if IE 7]><!--></a></li><!--<![endif]-->\n");
      out.write("                   \n");
      out.write("  \n");
      out.write("                    <li><a href=\"AddProducts.jsp\">Add Products></a></li>\n");
      out.write("                    \n");
      out.write("                    <li>  <a href=\"AddBrands.jsp\">ADD Brands<!--[if IE 7]><!--></a></li>\n");
      out.write("                   \n");
      out.write("                        \n");
      out.write("                    <li>   <a href=\"login.html\">Settings<!--[if IE 7]><!--></a></li><!--<![endif]-->\n");
      out.write("                    \n");
      out.write("                    <li><a href=\"\">Templates</a></li>\n");
      out.write("                    <li><a href=\"\">Custom details</a></li>\n");
      out.write("                    <li><a href=\"\">Contact</a></li>\n");
      out.write("                    </ul>\n");
      out.write("                    </div> \n");
      out.write("                    \n");
      out.write("    </body>\n");
      out.write("</html>\n");
      out.write("\n");
      out.write("    \n");
      out.write("   \n");
      out.write("                    \n");
      out.write("                    \n");
      out.write("                    \n");
      out.write("    <div class=\"center_content\">  \n");
      out.write("    \n");
      out.write("    \n");
      out.write("    \n");
      out.write("    <div class=\"left_content\">\n");
      out.write("    \n");
      out.write("    \t\t<div class=\"sidebar_search\">\n");
      out.write("            <form>\n");
      out.write("            <input type=\"text\" name=\"\" class=\"search_input\" value=\"search keyword\" onclick=\"this.value=''\" />\n");
      out.write("            <input type=\"image\" class=\"search_submit\" src=\"images/search.png\" />\n");
      out.write("            </form>            \n");
      out.write("            </div>\n");
      out.write("    \n");
      out.write("                    \n");
      out.write("                    \n");
      out.write("                    \n");
      out.write("    \n");
      out.write("    \n");
      out.write("    <div class=\"clear\"></div>\n");
      out.write("    </div> <!--end of main content-->\n");
      out.write("\t\n");
      out.write("    ");
      out.write("\n");
      out.write("\n");
      out.write("\n");
      out.write("<!DOCTYPE html>\n");
      out.write("<html>\n");
      out.write("    <head>\n");
      out.write("        <meta http-equiv=\"Content-Type\" content=\"text/html; charset=UTF-8\">\n");
      out.write("       <link href=\"style/style.css\" rel=\"stylesheet\" type=\"text/css\" />\n");
      out.write("    </head>\n");
      out.write("    <body>\n");
      out.write("         \n");
      out.write("    <div class=\"footer\">\n");
      out.write("    \n");
      out.write("    \t<div class=\"left_footer\">IN ADMIN PANEL | Developed By Rohit Singh And Sweta Suman</a></div>\n");
      out.write("    \t<div class=\"right_footer\"><a href=\"http://indeziner.com\"><img src=\"images/indeziner_logo.gif\" alt=\"\" title=\"\" border=\"0\" /></a></div>\n");
      out.write("    \n");
      out.write("    </div>\n");
      out.write("    </body>\n");
      out.write("</html>\n");
      out.write("\n");
      out.write("\n");
      out.write("</div>\t\t\n");
      out.write("</body>\n");
      out.write("</html>\n");
    } catch (Throwable t) {
      if (!(t instanceof SkipPageException)){
        out = _jspx_out;
        if (out != null && out.getBufferSize() != 0)
          out.clearBuffer();
        if (_jspx_page_context != null) _jspx_page_context.handlePageException(t);
        else throw new ServletException(t);
      }
    } finally {
      _jspxFactory.releasePageContext(_jspx_page_context);
    }
  }
}
