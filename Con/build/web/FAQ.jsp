<%-- 
    Document   : FAQ
    Created on : Aug 6, 2014, 10:31:10 AM
    Author     : rohitsingh
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
<title>FAQ's</title>
<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
    <link rel="stylesheet" type="text/css" href="http://cdn.webrupee.com/font">
    <script src=http://cdn.webrupee.com/js type=”text/javascript”></script>
    <script type="text/javascript">
        
        $(function () {
    $('table.table tr').click(function () {
        window.location.href = $(this).data('url');
    });
})
    </script>
        <script>
$(document).ready(function(){
  
    
     $("#myimg").fadeIn("slow");
     $("#myimg").fadeIn(5000);
    $("#flip").click(function(){
    $("#panel").slideDown("slow");
  });
  
});
</script>
<link href="style/style.css" rel="stylesheet" type="text/css" />
<style type="text/css">
   body {
  font-family: Arial, Verdana, sans-serif;
  color: #111111;}
table {
  width: 600px;}
th, td {
  padding: 7px 10px 10px 10px;}
th {
  text-transform: uppercase;
  letter-spacing: 0.1em;
  font-size: 90%;
  border-bottom: 2px solid #111111;
  border-top: 1px solid #999;
  text-align: left;}
tr.even {
  background-color: #efefef;}
tr:hover {
  background-color: #c3e6e5;}
.money {
  text-align: right;}
</style>
<style> 
#panel,#flip
{
padding:5px;
text-align:center;

}
#panel
{
padding:50px;
display:none;
}
</style>
</head>
<body>
<center>
    <%@include file="User.jsp" %> 
    <div class="body-wrapper">
        <span id="lblquesans"><h2 class='acc_trigger' name=6 id=6><a href='#'>Do I need to register on your website?</a></h2><div class='acc_container'><div class='block'><p>Yes. We require you to register on our site to make a purchase. You cannot shop/purchase anything using anonymous checkout on www.whitemart.in. However registration on www.whitemart.in is a very simple and quick process. It normally takes less then a minute to register on our site.</p></div></div><h2 class='acc_trigger' name=5 id=5><a href='#'>How do I place an order on your website?</a></h2><div class='acc_container'><div class='block'><p>Placing an order on www.whitemart.in is a really simple process and doesn’t take more than a minute. Here are a few quick steps to shop on our site:

a. Type www.shoponline.in on your web browser and hit enter 
b. You can browse by product categories, promotions or products displayed on the homepage. You can also use the search bar to find any product quickly without browsing through the categories. 
c. Once you have identified the product you wish to purchase, click on “Add to Cart” button if you wish to buy multiple products, then click on “Continue Shopping” and follow the same steps as mentioned above. 
d. If you are a registered member on www.shoponline.in, you can log in with your username and password or else you would be required to registered with www.whitemart.in to proceed further. 
e. Click on “Check Out” Once you have the required products in your shopping cart to proceed further to complete the order information. 
f. You would be required to fill the “Shipping & Billing Address” for the products you have added to your shopping cart. 
g. Choose the “Payment Options” you would like to use to make the payment from the various options available & click on “Pay Now." 
h. Once you choose the available “Payment Option” follow the instructions that appears on your screen to complete the ordering process. 
i. Once the payment process is completed, you will get an order number for your transaction. 
j. We normally ship all orders within 4 –5 working days depending on the availability. 
k. Call us on 99903049966
 if you face any problem in placing the order online. Our customer care specialist would be really happy to assist you. 
l. I have placed an order for an item. How long does it take to deliver? 


All Orders from www.shoponline.in are delivered within 4 - 5 working days of placing an order unless specified. However delivery is not made on Sundays and public holidays since there are no courier services available on these days. In case of natural calamities and disasters the order can be delayed further. We do not take responsibility for delay in such cases.</p></div></div><h2 class='acc_trigger' name=4 id=4><a href='#'>What should I do when I get the wrong merchandise instead of my order?</a></h2><div class='acc_container'><div class='block'><p>We will make best possible effort to ensure that the correct Order is delivered to your shipping address. However if there is a mismatch in the Order received at your end, we will arrange for the incorrect item to be picked up from your end and redispatch the same without any additional cost.

Please contact Customer Support in case of wrong Order received. We apologise in advance for any such errors.</p></div></div><h2 class='acc_trigger' name=3 id=3><a href='#'>Can I cancel a confirmed order?</a></h2><div class='acc_container'><div class='block'><p>Cancellations will only be considered if the request is made within 24-48 hours of placing an order for selected products. If the order is already under process with us or with the third party vendor, then the request may be rejected. For products listed under special Events ; Promotions (Rakhi, Diwali, Valentine etc…) and Express Delivery, cancellations will not be entertained.

Cancellation is only permissible in case the quality of the product delivered is not good [Product delivered is stale, incomplete etc]. In such cases report the issue to customer care immediately through our feedback section.

In case of receipt of damaged or defective items, please report the same to our customer care center by mentioning the same on our feedback section. The request can only be entertained once the merchant has checked and determined the same at his own end.</p></div></div><h2 class='acc_trigger' name=2 id=2><a href='#'>What are the different stages in Order Processing?</a></h2><div class='acc_container'><div class='block'><p>You can check the order status by visiting the “My Account” link in case of registered member. Please follow the steps mentioned as under:

Step 1: : Click on “Order Status“ Link on the homepage. For registered members it will ask to login. Once logged in, It shall display the orders you have placed with current status. 
Step 2: Click on the on the Order numbers under the Order ID section to know the details of the order. There are various stages the order goes through before reaching you at the desired destination. 


Various Stages of the order that it goes through are:
1. Order under Process: This is the first state of any order. 
2. OUP waiting for gateway response: This is the state which indicates that the payment has not been processed by the Gateway. In case you log out at time of making payment on the payment gateway page or the internet connection is lost while processing payment then this state would appear for the order. 
3. Payment Received: This confirms that the payment for the order has been accepted. 
4. To be verified: When the payment is made through bank cards, pay cards or any of redemption options, the orders are verified for authenticity. 
5. Shipped: The order is shipped through our third party Courier partners. The details of the shipment are mentioned on the “Variables Updated” section in the order form. 
6. Order cancelled: An Order may be cancelled due to variety of reasons. The reason is mentioned in the “Variables Updated“ column on the order form. 
7. Request for replacement: This state appears when an order has been sent for replacement. Majority of the products listed on the site are directly owned by edigiworld. 
8. Reshipped: The order has been reshipped by us or the merchant. The details of the shipment are mentioned in the “Variables Updated “column on the order form. 
9. Refunded: The payment for the order has been refunded. The refund is processed usually on the same mode on which the payment is made. However in case of COD [cash of delivery] the refund is processed via cheque mode. 
10. Repair: This indicates that the product has been forwarded for Repair to the merchant. 
11. Sales return: The products shipped have been returned by the consignee back to the seller.</p></div></div><h2 class='acc_trigger' name=1 id=1><a href='#'>Why am I not able to make payments online with my credit card?</a></h2><div class='acc_container'><div class='block'><p>Payment error page is displayed when there is a connection lapse between the payment gateway and the website or the internet connection is interrupted during payment transaction or possibly some required fields are not entered correctly.

You can follow the steps below when you encounter such issues related to payment errors:

Please report this error on our feedback link immediately. Please mention the following information for assistance.

1. Name on the card
2. Type and name of the card
3. Date of transaction
4. Amount paid

This usually takes up to 36 to 48 hours to resolve.
An order that has not been placed is reflecting on the bank card statement:
In such cases we might require a scanned copy of the bank card statement. Please contact our customer care for assistance.</p></div></div></span>
     
    </div>
      <%@include file="UserFooter.jsp" %>
</center>
    </div>
    </body>
</html>
