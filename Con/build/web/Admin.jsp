<%-- 
    Document   : Admin
    Created on : Jul 25, 2014, 4:01:01 PM
    Author     : rohitsingh
--%>

<%@page import="java.util.ArrayList"%>
<%@page import="Model.Category"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title> ADMIN PANEL </title>
<link rel="stylesheet" type="text/css" href="style.css" />
<%
HttpSession s=request.getSession();
if(s.isNew()){
RequestDispatcher rd=request.getRequestDispatcher("login.html");
rd.forward(request, response);
}
%>


</head>
<body>
<%@ include file="AdminHeader.jsp" %>
    
   
                    
                    
                    
    <div class="center_content">  
    
    
    
    <div class="left_content">
    
    		<div class="sidebar_search">
            <form>
            <input type="text" name="" class="search_input" value="search keyword" onclick="this.value=''" />
            <input type="image" class="search_submit" src="images/search.png" />
            </form>            
            </div>
    
                    
                    
                    
    
    
    <div class="clear"></div>
    </div> <!--end of main content-->
	
    <%@ include file="AdminFoter.jsp" %>

</div>		
</body>
</html>
