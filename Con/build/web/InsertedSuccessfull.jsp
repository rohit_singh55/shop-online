<%-- 
    Document   : InsertedSuccessfull
    Created on : Jul 30, 2014, 5:57:36 PM
    Author     : rohitsingh
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
   <link rel="stylesheet" type="text/css" href="style.css" />
<%
HttpSession s=request.getSession();
if(s.isNew()){
RequestDispatcher rd=request.getRequestDispatcher("login.html");
rd.forward(request, response);
}
%>


</head>
<body>
<%@ include file="AdminHeader.jsp" %>
<div class="center_content">
    
        <h1>Inserted Successfully!!</h1>
        </div> <!--end of main content-->
	
    <%@ include file="AdminFoter.jsp" %>


    </body>
</html>
