<%-- 
    Document   : User
    Created on : Aug 2, 2014, 6:53:40 PM
    Author     : rohitsingh
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@page session="false" %>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Shop Online</title>
    
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<link href="style/style.css" rel="stylesheet" type="text/css" />

</head>
<body>
<%
HttpSession gs=request.getSession(false);
if(gs==null){
RequestDispatcher rd=request.getRequestDispatcher("login.html");
rd.forward(request, response);
}
%>
<center>
    <div class="header">
    <div class="logo">Shop<strong>Online</strong></div>
    
   
  
    <div class="menuu">
        <div class="right_header">Welcome  <% out.println(request.getSession(false).getAttribute("firstname")); %> <a href="ViewCart.jsp">CART</a> | | <a href="Payment.jsp" >Checkout</a> || <a href="Logout.jsp" class="logout">Logout</a></div>
   
    </div>
    </div>
         <div class="menu">
      <ul class="solidblockmenu">
        <li><a href="Welcome.jsp">Home</a></li>
        <li><a href="UserCategories.jsp">Categories</a></li>
        <li><a href="UserGuide.jsp">User Guide</a></li>
        <li><a href="Review.jsp">Reviews</a></li>
        <li><a href="HotDeals.jsp">Hot Deals</a></li>
        <li><a href="FAQ.jsp">FAQ's</a></li>
      </ul>
      <div class="clear"></div>
    </div>
  </div>
     <div class="search">
      <form action="UserSearch.jsp" method="get">
    <div class="search-text"> Search the world of shopping!&nbsp;
        <input type="text" size="40" name="srch" />
      &nbsp;&nbsp;</div>
    <div style="float:left; margin-left:5px; margin-top:10px;">
      <input type="submit" src="images/search.jpg" />
    </div>
      </form>
  </div>
  <div class="hot-search">
    <div class="hot-search-text"> Hot Searches: Apple iPod Nano, Hugo Boss Clothing, HD DVD Player, DSLR Camera, Toshiba 32&quot; LCD TV, Unsecured Loans, Excersise Machines..</div>
  </div>
    </body>
</html>
