<%-- 
    Document   : Category.jsp
    Created on : Jul 29, 2014, 10:56:33 AM
    Author     : rohitsingh
--%>

<%@page import="java.sql.ResultSet"%>
<%@page import="java.sql.PreparedStatement"%>
<%@page import="java.sql.DriverManager"%>
<%@page import="java.sql.Connection"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Manage Categories</title>
        <link rel="stylesheet" type="text/css" href="style.css" />
    </head>
    <body>
       <%@ include file="AdminHeader.jsp" %>
         <div class="right_content">            
        
    <h2>Categories Settings</h2> 
                    
       <%
        Class.forName("com.mysql.jdbc.Driver");
          //  String connectionurl="jdbc:sqlserver://u5b42g1a2g.database.windows.net:1433"+";"+"database=ProjectDB"+";"+"user=rohit47@u5b42g1a2g"+";"+"password=@Sumaniya94623"+";"+"encrypt=true"+";"+"hostNameInCertificate=*.database.windows.net"+";"+"loginTimeout=30"+";"+"ssl=require";
      Connection connection = DriverManager
		.getConnection("jdbc:mysql://localhost:3306/com","root", "root");
          
         String select="Select * from category";
          PreparedStatement preparedStatement=connection.prepareStatement(select);
         
                  
        
          ResultSet rs=preparedStatement.executeQuery(); 
       %>             
<table id="rounded-corner" summary="2007 Major IT Companies' Profit">
    <thead>
    	<tr>
        	
            <th scope="col" class="rounded">Category Id</th>
            <th scope="col" class="rounded">Category Name</th>
            <th scope="col" class="rounded">Edit</th>
            <th scope="col" class="rounded-q4">Delete</th>
        </tr>
    </thead>
        <tfoot>
    	<tr>
        	<td colspan="4" class="rounded-foot-left"><em>Please click the below button to manage category</em></td>
        	<td class="rounded-foot-right">&nbsp;</td>

        </tr>
    </tfoot>
    <tbody>
        <% while(rs.next()){%>
    	<tr>
        	<TD> <%= rs.getString(1) %></td>
                <TD> <%= rs.getString(2) %></TD>
           

                <td><a href="EditCategory.jsp?id=<%= rs.getString(1) %>"><img src="images/user_edit.png" alt="" title="" border="0" /></a></td>
                <td><a href="DeleteCategory.jsp/?id=<%= rs.getString(1) %>" class="ask"><img src="images/trash.png" alt="" title="" border="0" /></a></td>
        </tr>
         <% } %>
        
    </tbody>
</table>

	 <a href="#" class="bt_green"><span class="bt_green_lft"></span><strong>Add new item</strong><span class="bt_green_r"></span></a>

     
     
        
     
                
    </body>
</html>
